#define ligne_1 2
#define ligne_2 3
#define ligne_3 4
#define ligne_4 5

#define colonne_1 8
#define colonne_2 9
#define colonne_3 10

#include <Servo.h>

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int pos = 0;    // variable to store the servo position


void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);

  // on configure les lignes en sortie
  pinMode(ligne_1, OUTPUT);
  pinMode(ligne_2, OUTPUT);
  pinMode(ligne_3, OUTPUT);
  pinMode(ligne_4, OUTPUT);

  // on configure les colonnes en entrée avec pull-up
  pinMode(colonne_1, INPUT_PULLUP);
  pinMode(colonne_2, INPUT_PULLUP);
  pinMode(colonne_3, INPUT_PULLUP);

  myservo.attach(11);  // attaches the servo on pin 9 to the servo object
}

void loop() {
  // put your main code here, to run repeatedly:

  // créer 3 variables pour connaître l'état des 3 colonnes
  int etat_colonne_1 = HIGH;
  int etat_colonne_2 = HIGH;
  int etat_colonne_3 = HIGH;

  // on met la ligne_1 à 0V et les autres à 5V
  digitalWrite(ligne_1, LOW);
  digitalWrite(ligne_2, HIGH);
  digitalWrite(ligne_3, HIGH);
  digitalWrite(ligne_4, HIGH);

  // on récupère l'état des colonnes
  etat_colonne_1 = digitalRead(colonne_1);
  etat_colonne_2 = digitalRead(colonne_2);
  etat_colonne_3 = digitalRead(colonne_3);

  // si la colonne_1 est à LOW, le bouton 1 est pressé
  if(etat_colonne_1 == LOW) {
    envoi_bouton_presse_sur_port_serie('1');
  }

  // si la colonne_2 est à LOW, le bouton 2 est pressé
  if(etat_colonne_2 == LOW) {
    envoi_bouton_presse_sur_port_serie('2');
  }

  // si la colonne_3 est à LOW, le bouton 3 est pressé
  if(etat_colonne_3 == LOW) {
    envoi_bouton_presse_sur_port_serie('3');
  }

  // on met la ligne_2 à 0V et les autres à 5V
  digitalWrite(ligne_1, HIGH);
  digitalWrite(ligne_2, LOW);
  digitalWrite(ligne_3, HIGH);
  digitalWrite(ligne_4, HIGH);

  // on récupère l'état des colonnes
  etat_colonne_1 = digitalRead(colonne_1);
  etat_colonne_2 = digitalRead(colonne_2);
  etat_colonne_3 = digitalRead(colonne_3);

  // si la colonne_1 est à LOW, le bouton 4 est pressé
  if(etat_colonne_1 == LOW) {
    envoi_bouton_presse_sur_port_serie('4');
  }

  // si la colonne_2 est à LOW, le bouton 5 est pressé
  if(etat_colonne_2 == LOW) {
    envoi_bouton_presse_sur_port_serie('5');
  }

  // si la colonne_3 est à LOW, le bouton 6 est pressé
  if(etat_colonne_3 == LOW) {
    envoi_bouton_presse_sur_port_serie('6');
  }

  // on met la ligne_3 à 0V et les autres à 5V
  digitalWrite(ligne_1, HIGH);
  digitalWrite(ligne_2, HIGH);
  digitalWrite(ligne_3, LOW);
  digitalWrite(ligne_4, HIGH);

  // on récupère l'état des colonnes
  etat_colonne_1 = digitalRead(colonne_1);
  etat_colonne_2 = digitalRead(colonne_2);
  etat_colonne_3 = digitalRead(colonne_3);

  // si la colonne_1 est à LOW, le bouton 7 est pressé
  if(etat_colonne_1 == LOW) {
    envoi_bouton_presse_sur_port_serie('7');
  }

  // si la colonne_2 est à LOW, le bouton 8 est pressé
  if(etat_colonne_2 == LOW) {
    envoi_bouton_presse_sur_port_serie('8');
  }

  // si la colonne_3 est à LOW, le bouton 9 est pressé
  if(etat_colonne_3 == LOW) {
    envoi_bouton_presse_sur_port_serie('9');
  }

  // on met la ligne_4 à 0V et les autres à 5V
  digitalWrite(ligne_1, HIGH);
  digitalWrite(ligne_2, HIGH);
  digitalWrite(ligne_3, HIGH);
  digitalWrite(ligne_4, LOW);

  // on récupère l'état des colonnes
  etat_colonne_1 = digitalRead(colonne_1);
  etat_colonne_2 = digitalRead(colonne_2);
  etat_colonne_3 = digitalRead(colonne_3);

  // si la colonne_1 est à LOW, le bouton * est pressé
  if(etat_colonne_1 == LOW) {
    envoi_bouton_presse_sur_port_serie('*');
  }

  // si la colonne_2 est à LOW, le bouton 0 est pressé
  if(etat_colonne_2 == LOW) {
    envoi_bouton_presse_sur_port_serie('0');
  }

  // si la colonne_1 est à LOW, le bouton # est pressé
  if(etat_colonne_3 == LOW) {
    envoi_bouton_presse_sur_port_serie('#');
  }
  for (pos = 0; pos <= 90; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15 ms for the servo to reach the position
    }
  for (pos = 90; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15 ms for the servo to reach the position
   }
  myservo.detach();
  delay(1000);
  
}

void envoi_bouton_presse_sur_port_serie(char bouton_caractere) {
  Serial.print(" Le bouton ");
  Serial.print(bouton_caractere);
  Serial.println(" est pressé ");
  Serial.println("");

}

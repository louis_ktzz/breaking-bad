# Créé par Louis.KRETZ, le 24/04/2023 en Python 3.7

import sqlite3

DATABASE = "myDB.db"

try :
    connecteur = sqlite3.connect("./"+DATABASE+".db")

    cursor = connecteur.cursor()

    cursor.execute(
        "CREATE TABLE if not exists `personnes` ( \
            `id`			INTEGER PRIMARY KEY, \
            `denomination`	varchar(20) not NULL, \
            `quantity`		varchar(20) not NULL, \
            CHECK (denomination <> ''), \
            CHECK (quantity <> ''), \
            UNIQUE(denomination) \
            )"
        )
    
    data = [
        ('Pansements', '30'),
        ('Compresses', '35')
        ]
                
    cursor.executemany("insert into personnes values(NULL, ?, ?)", data)
    connecteur.commit()
            
    request = "SELECT * FROM personnes"
    for id, denomination, quantity in cursor.execute(request).fetchall():
        print('id:'+str(id)+'   name:'+str(denomination)+'  quantity:'+quantity)
            
            
    cursor.close()
    connecteur.close()
except:
    print('erreur')
                
                

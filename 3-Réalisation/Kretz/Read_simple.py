import serial
import sqlite3

# Connexion à la base de données
connecteur = sqlite3.connect('myDB.db')
cursor = connecteur.cursor()

# Configuration de la communication avec l'Arduino
port = '/dev/ttyACM0'  # Port série utilisé par l'Arduino, à adapter selon votre configuration
baudrate = 9600  # Vitesse de communication, à adapter selon votre configuration
arduino = serial.Serial(port, baudrate)
while True :
    # Lecture du chiffre à partir de l'Arduino
    num = arduino.readline().decode().strip()

    # Requête pour chercher les produits et les prix correspondants dans la base de données
    query = "SELECT denomination, price FROM produits WHERE id = ?"
    cursor.execute(query, (num,))

    # Récupération des résultats et affichage
    resultats = cursor.fetchall()
    if resultats:
        print("Les produits correspondants sont :")
        for resultat in resultats:
            denomination, price = resultat
            print(f"{denomination} : {price} euros")
    else:
        print("Aucun produit correspondant à ce chiffre n'a été trouvé.")
# Créé par Louis.KRETZ, le 24/04/2023 en Python 3.7

import sqlite3

DATABASE = "myDB"

try :
    connecteur = sqlite3.connect("./"+DATABASE+".db")

    cursor = connecteur.cursor()

    cursor.execute(
        "CREATE TABLE if not exists `produits` ( \
            `id`			INTEGER PRIMARY KEY, \
            `denomination`	varchar(20) not NULL, \
            `quantity`		varchar(20) not NULL, \
            `price`		varchar(20) not NULL, \
            CHECK (denomination <> ''), \
            CHECK (quantity <> ''), \
            UNIQUE(denomination) \
            )"
        )

    data = [
        ('Pansements', '30', '9.44'),
        ('Compresses', '35', '9.99'),
        ('Produits désinfectants', '32', '9.99'),
        ('Thermomètre', '36', '9.99'),
        ('Crèmes hydratantes', '28', '9,99'),
        ('Crèmes solaires', '39', '9,99'),
        ('Baumes réparateurs', '30', '9,99'),
        ('Mousses à raser', '18', '9,99'),
        ('Lotions', '40', '9,99'),
        ('Préservatifs', '70', '9,99'),
        ('Crèmes lubrifiantes', '49', '9,99'),
        ('Tampons', '100', '9,99'),
        ('Serviettes hygiéniques', '100', '9,99'),
        ('Brosses à dent', '45', '9,99'),
        ('Fil dentaire', '20', '9,99'),
        ('Baume pour les lèvres', '21', '9,99'),
        ('Gel hydroalcoolique', '48', '9,99'),
        ('Masques', '200', '9,99'),
        ('Gants', '210', '9,99'),
        ('Laits premiers âges', '27', '9,99'),
        ('Couches', '80', '9,99'),
        ('Talc', '23', '9,99'),
        ('Biberons', '20', '9,99'),
        ('Tétines', '54', '9.44')

        ]

    cursor.executemany("insert into produits values(NULL, ?, ?,?)", data)
    connecteur.commit()

    request = "SELECT * FROM produits"
    for id, denomination, quantity, price in cursor.execute(request).fetchall():
        print('id:'+str(id)+'   name:'+denomination+'  quantity:'+quantity+'  price:'+price+'€')


    cursor.close()
    connecteur.close()
except:
    print('erreur')



# Créé par Louis.KRETZ, le 24/04/2023 en Python 3.7

import sqlite3

DATABASE = "myDB.db"

try :
    connecteur = sqlite3.connect("./"+DATABASE+".db")

    cursor = connecteur.cursor()

    cursor.execute(
        "CREATE TABLE if not exists `personnes` ( \
            `id`			INTEGER PRIMARY KEY, \
            `denomination`	varchar(20) not NULL, \
            `quantity`		varchar(20) not NULL, \
            CHECK (denomination <> ''), \
            CHECK (quantity <> ''), \
            UNIQUE(denomination) \
            )"
        )

    data = [
        ('Pansements', '30'),
        ('Compresses', '35'),
        ('Produits désinfectants', '32'),
        ('Thermomètre', '36'),
        ('Crèmes hydratantes', '28'),
        ('Crèmes solaires', '39'),
        ('Baumes réparateurs', '30'),
        ('Mousses à raser', '18'),
        ('Lotions', '40'),
        ('Préservatifs', '70'),
        ('Crèmes lubrifiantes', '49'),
        ('Tampons', '100'),
        ('Serviettes hygiéniques', '100'),
        ('Brosses à dent', '45'),
        ('Fil dentaire', '20'),
        ('Baume pour les lèvres', '21'),
        ('Gel hydroalcoolique', '48'),
        ('Masques', '200'),
        ('Gants', '210'),
        ('Laits premiers âges', '27'),
        ('Couches', '80'),
        ('Talc', '23'),
        ('Biberons', '20'),
        ('Tétines', '54')

        ]

    cursor.executemany("insert into personnes values(NULL, ?, ?)", data)
    connecteur.commit()

    request = "SELECT * FROM personnes"
    for id, denomination, quantity in cursor.execute(request).fetchall():
        print('id:'+str(id)+'   name:'+str(denomination)+'  quantity:'+quantity)


    cursor.close()
    connecteur.close()
except:
    print('erreur')



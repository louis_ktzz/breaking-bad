import sqlite3

# Connexion à la base de données
connecteur = sqlite3.connect('myDB.db')
cursor = connecteur.cursor()

# Saisie du chiffre à partir du clavier
num = input("Entrez un chiffre : ")

# Requête pour chercher les produits correspondants dans la base de données
query = "SELECT denomination, price FROM produits WHERE id = ?"
cursor.execute(query, (num,))

# Récupération des résultats et affichage
resultats = cursor.fetchall()
if resultats:
    print("Les produits correspondants sont :")
    for resultat in resultats:
        denomination, price = resultat
        print(f"{denomination} : {price} €")
else:
    print("Aucun produit correspondant à ce chiffre n'a été trouvé.")

# Fermeture de la connexion à la base de données
connecteur.close()
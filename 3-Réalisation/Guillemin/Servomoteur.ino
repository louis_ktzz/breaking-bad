#include <Servo.h>  // permet de faire appel à la bibliothèque servo

Servo monservo;    // crée une constante utilisable par la bibliothèque

int bouton = 8;    // bouton poussoir sur le PIN 8
int etatbouton = 0;   // variable représentant le bouton qui est fermé

void setup() {	
	pinMode (bouton, INPUT);   //le bouton est une entrée
      monservo.attach(9);       // servo moteur sur la PIN 9
}

void loop(){
	etatbouton = digitalRead (bouton);   // on lit l'etat du bouton (1 ou 0)


	if (etatbouton==HIGH){      // si ouverte alors laisser le courant
		monservo.write(90);  // servo moteur 90 degré
		delay(500);         // attendre 1.5 sec
		monservo.write (0);  // reviens a la position de depart
		delay(150);     //  attendre 150ms pour recommencer la boucle
		}
 }         
int trigPin = 8;
int echoPin = 7;
int LEDVERTEPin = 9;
int LEDROUGEPin = 10;
float v=331.5+0.6*20;
void setup()
{
    Serial.begin(115200);
    pinMode (trigPin, OUTPUT);
    pinMode (echoPin, INPUT);
    pinMode(LEDVERTEPin, OUTPUT);
    pinMode(LEDROUGEPin, OUTPUT);
}
float distanceM(){
  digitalWrite(trigPin, LOW);
  delayMicroseconds(3000);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(5000);
  digitalWrite(trigPin, LOW);
  float tUs = pulseIn(echoPin, HIGH);
  float t = tUs / 1000.0 / 1000.0 / 2;
  float d = t*v;
  return d*100;
}
void loop() 
{
  int d=distanceM();
  if (d<25) {
     Serial.println("présence d'objet"); 
     digitalWrite(LEDVERTEPin, HIGH);
    digitalWrite(LEDROUGEPin, LOW);
  } else {
    digitalWrite(LEDVERTEPin, LOW);
    digitalWrite(LEDROUGEPin, HIGH);
  }
  delay(200);
  }

  


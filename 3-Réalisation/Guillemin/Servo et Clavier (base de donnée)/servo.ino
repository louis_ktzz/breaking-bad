#include <Servo.h>

Servo myservo;  // Crée un objet servo

int pos = 0;    // Variable pour stocker la position actuelle du servo

void setup() {
  Serial.begin(9600);  // Initialise la communication série à 9600 bauds
  myservo.attach(11);   // Attache le servo au broche 9
}

void loop() {
  if (Serial.available() >0) 
  {
    char command = Serial.readStringUntil('\n');  // Lit le premier caractère reçu

    if (command == 'ok') 
    {
      // Si 'ok' est reçu, démarre le programme Sweep
      Serial.println("Programme Sweep en cours...");
      sweep();
    } else if (command == 'ko') {
      // Si 'ko' est reçu, arrête le programme Sweep
      Serial.println("Programme Sweep arrêté.");
    }
  }
}

void sweep() {
  for (pos = 0; pos <= 180; pos += 1) {
    myservo.write(pos);        // Déplace le servo à la position 'pos'
    delay(15);                 // Attente de 15 ms pour le mouvement
  }
  
  for (pos = 180; pos >= 0; pos -= 1) {
    myservo.write(pos);        // Déplace le servo à la position 'pos'
    delay(15);                 // Attente de 15 ms pour le mouvement
  }
}

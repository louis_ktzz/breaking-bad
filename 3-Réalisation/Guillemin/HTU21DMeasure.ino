#include <HTU21D.h>

HTU21D sensor;

void setup() {
  Serial.begin(9600);
  sensor.begin();
}

void loop() {
  if(sensor.measure()) {
    float temperature = sensor.getTemperature();
    
    Serial.print("Temperature (°C): ");
    Serial.println(temperature);
  
  }

  delay(2000);
}
